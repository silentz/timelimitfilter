package com.xinrong.filter;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.HRegionLocation;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

import java.io.IOException;
import java.util.Date;
import java.util.zip.CRC32;

/**
 * Created by xinrong3 on 14-5-28.
 */
public class TimeLimitFilterTest {

    static Configuration conf= HBaseConfiguration.create();
    static String zkQuorum="zk1.mars.grid.sina.com.cn,zk2.mars.grid.sina.com.cn,zk3.mars.grid.sina.com.cn,zk4.mars.grid.sina.com.cn,zk5.mars.grid.sina.com.cn";
    static String zkParent="/repost-hbase";
    static {
        conf.set(HConstants.ZOOKEEPER_QUORUM,zkQuorum);
        conf.set(HConstants.ZOOKEEPER_ZNODE_PARENT,zkParent);
    }


    @Test
    public void TestFilter(){
        String keys="3663681974115631\n" +
                "3664457319867350\n" +
                "3672884188499362\n" +
                "3675029793341857\n" +
                "3719490929651461\n" +
                "3701761703079671\n" +
                "3463996517028521\n" +
                "3461991505365510\n" +
                "3461822030069834\n" +
                "3458942090884559\n" +
                "3446958905831586\n" +
                "3444080325144954\n" +
                "3442995053591079\n" +
                "3718102066088217\n" +
                "3714553382604299\n" +
                "3719567098711798\n" +
                "3717408864505586\n" +
                "3717318515494935\n" +
                "3712228820125681\n" +
                "3714510651650534\n" +
                "3715246961346686\n" +
                "3715249670903501\n" +
                "3714811546001659\n" +
                "3714419537788334\n" +
                "3714405084987780\n" +
                "3714478497822525\n" +
                "3715293434072889\n" +
                "3714508096838532\n" +
                "3714539428424462\n" +
                "3713407850688561\n" +
                "3714810174526160\n" +
                "3714568512058397\n" +
                "3714554091861747\n" +
                "3714417814465481\n" +
                "3719584090009849\n" +
                "3715264942347483\n" +
                "3719495917246944\n" +
                "3715177042203161\n";


        String [] ids=keys.split("\\n");
        for(String s:ids){
//            System.out.println();
            testKeys(Long.parseLong(s));
        }
    }


    private void testKeys(long idlong){
        String tableName="repost";
        byte [] id=hashRow(idlong);
        try {
            HTable table=new HTable(conf, tableName);
            HConnection connection=table.getConnection();
            HRegionLocation location=connection.getRegionLocation(Bytes.toBytes(tableName), id, false);
            System.out.print(location);

            Get get1=new Get(id);
            get1.setFilter(new TimeLimitFilter(300));
            Result r1=table.get(get1);
            long time1=System.currentTimeMillis();
            r1=table.get(get1);
            System.out.println("time with filter100:"+(System.currentTimeMillis()-time1)+"\tresult:"+r1.rawCells().length);


            Get get2=new Get(id);
            FilterList filterList=new FilterList();
            filterList.addFilter(new TimeLimitFilter(300));
//            filterList.addFilter(new ColumnPaginationFilter(1000000,1000000));
            get2.setFilter(filterList);

            long time2=System.currentTimeMillis();
            Result r2=table.get(get2);
            System.out.println("time with filter3000:"+(System.currentTimeMillis()-time2)+"\tresult:"+r2.rawCells().length);


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static byte[] hashRow(long id) {
        byte[] raw = Bytes.toBytes(-id);
        byte[] rowKey = new byte[raw.length + 2];
        long scatter = getCrc32(raw);
        rowKey[0] = (byte) ((scatter & 0xFFFF) >> 8);
        rowKey[1] = (byte) (scatter & 0xFF);
        Bytes.putBytes(rowKey, 2, raw, 0, raw.length);
        return rowKey;
    }

    public static long getCrc32(byte[] b) {
        CRC32 crc = crc32Provider.get();
        crc.reset();
        crc.update(b);
        return crc.getValue();
    }

    private static ThreadLocal<CRC32> crc32Provider = new ThreadLocal<CRC32>(){
        @Override
        protected CRC32 initialValue() {
            return new CRC32();
        }
    };


    @Test
    public void timeCount(){
        Date date=new Date(1401333814289l);
        System.out.println(date);
    }
}
